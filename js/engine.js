$(function() {


// Фиксированный блок на странице Оформление заказа
if ($(window).width() > 1199) {
      $('.airSticky').airStickyBlock();
};




// Фиксированная шапка припрокрутке
$(window).scroll(function(){


  
  if ($(window).width() > 768) {
      var sticky = $('.header'),
          scroll = $(window).scrollTop();
      if (scroll > 100) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 768) {
      $('.header').removeClass('header-fixed'); 
    }
});


// Филтры в сайдбаре
$('.sidebar-item__header').click(function() {
  $(this).addClass('active');
  $(this).next('.sidebar-item__body').fadeToggle();
})



// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });




// Меню пользователя
$('.user-menu__btn').hover(function() {
  $(this).children('.user-menu').stop(true, true).fadeToggle();
})













// Banner slider
$('.banner').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: true,
    arrows: false,
    dots: true
   
});

// Brand slider
$('.brand-slider').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 7,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    fade: false,
    arrows: true,
    responsive: [
    {
        breakpoint: 1365,
        settings: {
            slidesToShow: 6,
        }
        },
        {
        breakpoint: 1199,
        settings: {
            slidesToShow: 5,
        }

        },
        {
        breakpoint: 991,
        settings: {
            slidesToShow: 4,
        }

        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 3,
        }

        },
        {
        breakpoint: 590,
        settings: {
            slidesToShow: 2,
        }

        },
        {
        breakpoint: 420,
        settings: {
            slidesToShow: 1,
        }

        },

    ]
   
});


// Меню в футере на мобильном
if ($(window).width() <= 768) {
  $('.footer-links .h3').click(function() {
    console.log('click')
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}

// "Показать ещё" в меню каталога
$('.catalog-menu__links--more').click(function(event) {
    event.preventDefault();
    $(this).parent().find('.catalog-menu__links--list').toggleClass('active');
    $(this).fadeToggle(0);
});



// Лайк продукта
$('.product-item__btn a').click(function() {
  $(this).toggleClass('active');
})





// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range


// Переключатель
$('.switch-btn').click(function (e, changeState) {
    if (changeState === undefined) {
        $(this).toggleClass('switch-on');
    }
    if ($(this).hasClass('switch-on')) {
        $(this).trigger('on.switch');
    } else {
        $(this).trigger('off.switch');
    }
});



var productSlider = $('.card-img__slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  
  asNavFor: '.card-img__slider--nav'
});


$('.card-img__slider--nav').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.card-img__slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  vertical: true,
  responsive: [
  {
    breakpoint: 550,
    settings: {
      vertical: false,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 3,
      vertical: false,
    }
  },
  ]
});


 // Стилизация селектов
$('select').styler();



$('.tabs-nav a:not(.scroll)').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});



// Показать подробности/Скрыть подробности
$('.history-more__show').click(function(event) {
    $(this).parent().parent().next('.history-table__row--hidden').toggleClass('active');
    event.preventDefault();
    this.textContent = this.textContent === 'Показать подробности' ? 'Скрыть подробности' : 'Показать подробности';
});


// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();







// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})







// Скролл
$(".scroll").on("click","a", function (event) {
    event.preventDefault(); //опустошим стандартную обработку
    $('.scroll a').removeClass('active');
    $(this).addClass('active')
    var id  = $(this).attr('href'), //заберем айдишник блока с параметром URL
      top = $(id).offset().top; //определим высоту от начала страницы до якоря
  $('body,html').animate({scrollTop: top - 76}, 1000); //сделаем прокрутку за 1 с
  });






// MMENU
if ($(window).width() < 768) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}else {
  //Показать/скрыть каталог
$('.catalog-btn').click(function(event) {
  event.preventDefault();
  $(this).toggleClass('active');
  $('.catalog-menu').fadeToggle();
});
}




})